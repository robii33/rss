module gitlab.com/robii33/rss

go 1.19

require (
	github.com/go-chi/chi v1.5.5
	github.com/joho/godotenv v1.5.1
)

require github.com/go-chi/cors v1.2.1
